

/**
 * Implements the QuizAttempt interface
 *
 * @author Julian Fenner
*/

public class QuizAttemptImpl implements QuizAttempt, java.io.Serializable{
	private static final long serialVersionUID = 6L;
	private int quizId;
	private int quizScore;
	
	public QuizAttemptImpl(int quizId, int quizScore){
		this.quizId = quizId;
		this.quizScore = quizScore;
	}
	
	@Override
	public int getQuizId(){
		return quizId;
	}
	
	@Override
	public int getQuizScore(){
		return quizScore;
	}
}