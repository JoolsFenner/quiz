

import java.rmi.Naming;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.rmi.RMISecurityManager;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.*;


public class QuizServerLauncher{
	QuizService server;
	
	private void shutDownMenu() throws RemoteException{
		boolean confirmed = false;
		
		do{
			System.out.println();
			System.out.print("Enter 1 to save data exit: ");
			System.out.print("Enter 2 to exit without saving: ");
			String userOption = System.console().readLine();
			
			switch(userOption){
				case "1":	server.flush();
							confirmed = true;
							break;
				case "2":	confirmed = true;
							System.out.println("Exiting without saving");
							break;
				default:	System.out.println("Invalid input");
							break;
			}
			
		} while(!confirmed);
	
		System.out.println("Goodbye");
		System.exit(0);
		
	}
	
	private void launch() throws RemoteException{

		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}
		try {
			LocateRegistry.createRegistry(1099);
			server = new QuizServer();
			String registryHost = "//localhost/";
			String serviceName = "quiz";
			Naming.rebind(registryHost + serviceName, server);
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
		System.out.println();
		System.out.println("***QUIZ SERVER***");
		shutDownMenu();
	}
	
	public static void main(String[] args) throws RemoteException{
		new QuizServerLauncher().launch();	
	}

}