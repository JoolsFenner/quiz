

import java.util.List;
import java.util.ArrayList;

/**
 * Implements the Quiz Interface
 *
 * @author Julian Fenner
*/
public class QuizImpl implements Quiz, java.io.Serializable{
	private static final long serialVersionUID = 5L;
	private String quizName;
	private int quizId;
	private List<Question> questions;
	
	public QuizImpl(String quizName){
		this.quizName = quizName;
		this.questions = new ArrayList<Question>();
	}
	
	@Override
	public void addQuestionAndAnswers(String question, String answer1, String answer2, String answer3, String answer4, int correctAnswer){
		if(correctAnswer < 1 || correctAnswer > 4) throw new IllegalArgumentException("Error. The correctAnswer argument " + correctAnswer + " is outside of the legal range 1 - 4");
		Question q = new QuestionImpl(question);
		q.setPotentialAnswer(1, answer1);
		q.setPotentialAnswer(2, answer2);
		q.setPotentialAnswer(3, answer3);
		q.setPotentialAnswer(4, answer4);
		q.setCorrectAnswer(correctAnswer);
		questions.add(q);
	}
	
	@Override
	public int setId(int id){
		this.quizId = id;
		return quizId;
	}
	
	@Override
	public int getId(){
		return quizId;
	}
	
	@Override
	public List<Question> getQuestions(){
		return questions;
	}
	
	public String getQuizName(){
		return quizName;
	}
			
}