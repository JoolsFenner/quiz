

import java.util.List;
import java.util.ArrayList;

/**
 * Implements the Player interface
 *
 * @author Julian Fenner
*/

public class PlayerImpl implements Player, java.io.Serializable{
	private static final long serialVersionUID = 3L;
	private String name;
	private int id;
	private List<QuizAttempt> quizAttempts;
	
	public PlayerImpl(String name, int id){
		this.name = name;
		this.id = id;
		this.quizAttempts = new ArrayList<QuizAttempt>();
	}
	
	@Override
	public void addQuizAttempt(int quizId, int quizScore){
		quizAttempts.add(new QuizAttemptImpl(quizId, quizScore));
	}
	
	@Override
	public int getId(){
		return id;
	}	
	
	@Override
	public String getName(){
		return name;
	}
	
	@Override
	public List<QuizAttempt> getQuizAttempts(){
		if(quizAttempts.isEmpty()) return null;
		return quizAttempts;
	}
}