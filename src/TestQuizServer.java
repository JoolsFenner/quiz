

import org.junit.Test;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.rules.ExpectedException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.io.*;

/**
* Tests the QuizService implementation
*
*/
public class TestQuizServer {
	
	QuizService testQuizServer;
	Quiz testQuiz1;
	
	//
	String testFlushFileName;
	File testFlushFile;
	
	@Before
	public void buildUpMain() throws RemoteException{
		this.testQuizServer = new QuizServer("TestServerData.ser");
		this.testFlushFileName = "TestFlushData.ser";
		this.testFlushFile = new File(testFlushFileName);
		
		testQuiz1 = new QuizImpl("Test Quiz 1");
		testQuiz1.addQuestionAndAnswers("Q1 ", "A1", "A2", "A3", "A4", 1);	
		testQuiz1.addQuestionAndAnswers("Q2 ", "A1", "A2", "A3", "A4", 3);	
		testQuiz1.addQuestionAndAnswers("Q3 ", "A1", "A2", "A3", "A4", 4);	
	}
	
	@After
	public void tearDownMain() throws RemoteException {
		if(testFlushFile.exists()) testFlushFile.delete();
	}
	
	// int addQuiz(Quiz quiz) tests
	
	@Test
	public void testAddQuiz_CreatedAndAddedToCollection() throws RemoteException{
		testQuizServer.addQuiz(testQuiz1);
		assertEquals(testQuizServer.getQuizzes().size(), 1);
	}
	
	@Test
	public void testAddQuiz_IdAllocationWorking() throws RemoteException{
		int id1 = testQuizServer.addQuiz(testQuiz1);
		int id2 = testQuizServer.addQuiz(testQuiz1);
		assertEquals((id2-id1), 1);
	}
	
	@Test(expected = NullPointerException.class)
	public void testAddQuiz_NullQuiz() throws RemoteException {
		Quiz nullQuiz = null;
		testQuizServer.addQuiz(nullQuiz);
	}
	
	
	
	// List<Quiz> getQuizzes() tests
	
	@Test
	public void testGetQuizzes() throws RemoteException {
		testQuizServer.addQuiz(testQuiz1);
		testQuizServer.addQuiz(testQuiz1);
		testQuizServer.addQuiz(testQuiz1);
		assertEquals(testQuizServer.getQuizzes().size(), 3);
		
	}
	
	
	// Quiz getQuiz(int quizId) tests
	
	@Test
	public void testGetQuiz() throws RemoteException{
		int id1 = testQuizServer.addQuiz(testQuiz1);
		Quiz testQuiz1Get = testQuizServer.getQuiz(id1); 
		assertEquals(testQuiz1, testQuiz1Get);
	}
	
	@Test
	public void testGetQuiz_noSuchQuiz() throws RemoteException{
		assertEquals(null, testQuizServer.getQuiz(999));
	}
	
	
	//Integer[] getQuizzesKeys() tests
	
	@Test
	public void testGetQuizzesKeys() throws RemoteException{
		Integer[] testArray = new Integer[]{1,2,3};
		testQuizServer.addQuiz(testQuiz1);
		testQuizServer.addQuiz(testQuiz1);
		testQuizServer.addQuiz(testQuiz1);
		assertArrayEquals(testQuizServer.getQuizzesKeys(), testArray);
	}

	
	@Test
	public void testGetQuizzesKeys_testNullReturn() throws RemoteException{
		assertArrayEquals(testQuizServer.getQuizzesKeys(), null);
	}
		
		
	//int createPlayer(String name) tests
	
	@Test(expected = IllegalArgumentException.class)
	public void testCreatePlayer_emptyString() throws RemoteException {
		testQuizServer.createPlayer("");
	}
	
	
	//List<Player> getPlayers() tests
	
	@Test
	public void testGetPlayers() throws RemoteException {
		testQuizServer.createPlayer("player1");
		testQuizServer.createPlayer("player2");
		testQuizServer.createPlayer("player3");
		assertEquals(testQuizServer.getPlayers().size(), 3);	
	}
	
	
	// Player getPlayer(int playerId) tests
	
	@Test
	public void testGetPlayer() throws RemoteException{
		String expectedName = "player1";
		int id1 = testQuizServer.createPlayer(expectedName);
		assertEquals(expectedName, testQuizServer.getPlayer(id1).getName());
	}
	
	@Test
	public void testGetPlayer_noSuchPlayer() throws RemoteException{
		assertEquals(null, testQuizServer.getPlayer(999));
	}
	
	
	// void addPlayerScore(int playerId, int quizId, int playerScore) tests

	@Test(expected = IllegalArgumentException.class)
	public void testAddPlayerScore_noSuchPlayer() throws RemoteException {
		int validQuizId = testQuizServer.addQuiz(testQuiz1);
		int invalidPlayerId = testQuizServer.createPlayer("testPlayer") + 10;
		
		testQuizServer.addPlayerScore(invalidPlayerId, validQuizId, 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testAddPlayerScore_noSuchQuiz() throws RemoteException {
		int validPlayerId = testQuizServer.createPlayer("testPlayer");
		int invalidQuizId = testQuizServer.addQuiz(testQuiz1)+ 10;
		
		testQuizServer.addPlayerScore(validPlayerId, invalidQuizId, 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testAddPlayerScore_scoreOutsideOfUpperRange() throws RemoteException {
		int validPlayerId = testQuizServer.createPlayer("testPlayer");
		int validQuizId = testQuizServer.addQuiz(testQuiz1);
		int testQuiz1IllegalScore = testQuiz1.getQuestions().size() + 1;
		
		testQuizServer.addPlayerScore(validPlayerId, validQuizId, testQuiz1IllegalScore);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testAddPlayerScore_scoreOutsideOfLowerRange() throws RemoteException {
		int validPlayerId = testQuizServer.createPlayer("testPlayer");
		int validQuizId = testQuizServer.addQuiz(testQuiz1);
		testQuizServer.addPlayerScore(validPlayerId, validQuizId, -1);
	}
	
	//Integer[] getPlayersKeys() test
	@Test
	public void testGetPlayersKeys() throws RemoteException{
		Integer[] testArray = new Integer[]{1,2,3};
		testQuizServer.createPlayer("testPlayer");
		testQuizServer.createPlayer("testPlayer");
		testQuizServer.createPlayer("testPlayer");
		assertArrayEquals(testQuizServer.getPlayersKeys(), testArray);
	}
	
	//void flush() test
	@Test
	public void testFlush() throws RemoteException{
		QuizService tempQuizServer = new QuizServer(testFlushFileName);
		
		int tempPlayer1 = tempQuizServer.createPlayer("tempPlayer1");
		int tempPlayer2 = tempQuizServer.createPlayer("tempPlayer2");
		int tempQuiz1 = tempQuizServer.addQuiz(testQuiz1);
		int tempQuiz2 = tempQuizServer.addQuiz(testQuiz1);
		
		tempQuizServer.flush();
		
		QuizService tempQuizServer2 = new QuizServer(testFlushFileName);
		
		assertEquals(tempQuizServer.getPlayer(tempPlayer1).getName(),tempQuizServer2.getPlayer(tempPlayer1).getName()); 
		assertEquals(tempQuizServer.getPlayer(tempPlayer2).getName(),tempQuizServer2.getPlayer(tempPlayer2).getName()); 
		assertEquals(tempQuizServer.getQuiz(tempQuiz1).getQuizName(),tempQuizServer.getQuiz(tempQuiz1).getQuizName()); 
		assertEquals(tempQuizServer.getQuiz(tempQuiz2).getQuizName(),tempQuizServer.getQuiz(tempQuiz2).getQuizName()); 
	}
}
	