

/**
 * A class to represent questions
 *
 * @author Julian Fenner
*/
public interface Question{
	/**
	 * Sets a potential answer to a question
	 *
	 * @param answerNumber the number of the answer
	 * @param answer the answer
	*/
	void setPotentialAnswer(int answerNumber, String answer);
	
	/**
	 * Sets the correct answer
	 *
	 * @param correctAnswer the id of the correct answer
	*/
	void setCorrectAnswer(int correctAnswer);
	
	/**
	 * Returns a question
	 *
	 * @return the question
	*/
	String getQuestion();
	
	/**
	 * Returns all potential answers to the question
	 *
	 * @return all potential answers
	*/
	String getAnswers();
	
	/**
	 * Returns the correct answer
	 *
	 * @return the id of the correct answer
	*/	
	int getCorrectAnswer();

}