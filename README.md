Quiz
====

A networked quiz game with three components.  
1. A quiz server which stores the quizzes and player data.
2. A setup client which allows new quizzes to be setup and other quizzes on the server to be looked at.
3. A player client which allows a player to choose/setup a profile,  choose and play a quiz and look at their previous quiz scores.


### To Run

1. Compile
javac QuizServerLauncher.java,  javac PlayerClientImpl.java, javac SetUpClientImpl.java 

2. Create server stub 
rmic QuizServer

3. Launch server
java -Djava.security.policy=server.policy QuizServerLauncher

4. Launch either setup client or player client
java -Djava.security.policy=client.policy SetUpClientImpl
	or
java -Djava.security.policy=server.policy PlayerClientImpl	


### Test Data Setup

If you want to quickly load some test data into the server copy ServerData.txt from the root directory into src changing the suffix to .ser.



### Warning

Security policies for client and server set to "permission java.security.AllPermission".  For local network testing purposes only!

### Javadocs
Javadocs updated through commandline with javadoc -d doc src/*.java