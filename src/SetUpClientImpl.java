

import java.util.List;
import java.util.ArrayList;
import java.rmi.NotBoundException;
import java.rmi.Naming;
import java.rmi.Remote;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.io.IOException;

/**
 * Implements the SetUpClient interface
 *
 * @author Julian Fenner
*/
public class SetUpClientImpl extends ClientTools implements SetUpClient {
	private static final long serialVersionUID = 1L;
	private Remote service;
	private QuizService quizService;
	
	public SetUpClientImpl()throws NotBoundException, IOException{
		this.service = Naming.lookup("//127.0.0.1:1099/quiz");
		this.quizService = (QuizService) service;
	}
	
	@Override
	public void createNewQuiz() throws RemoteException{
		System.out.println();
		System.out.println("***CREATE NEW QUIZ***");
		System.out.println();
		String quizName = getAndVerifyStringInput("Enter quiz name");	
		Quiz quiz = new QuizImpl(quizName);
		
		boolean finishedInputting = false;
		int questionNumber = 1;
		do{
		//each loop collects a question, 4 possible answers and the correct answer
			StringBuilder builder = new StringBuilder();

			String question = getAndVerifyStringInput("Enter question number " + questionNumber); 
			builder.append("Question " + questionNumber + " " + question + System.getProperty("line.separator"));
			questionNumber++;
			
			String[] possibleAnswers = new String[4];
			for(int i = 0; i < 4; i++){
				possibleAnswers[i] = getAndVerifyStringInput("Enter possible answer no " + (i+1));
				builder.append("Answer " + possibleAnswers[i] + " " + (i+1) +  System.getProperty("line.separator"));
			}
			
			System.out.println();
			System.out.println(builder);
			System.out.print("Enter the number of the correct answer: ");
			int correctAnswer = getAndVerifyNumber(new Integer[]{1,2,3,4});
			
			quiz.addQuestionAndAnswers(question, possibleAnswers[0], possibleAnswers[1], possibleAnswers[2],possibleAnswers[3], correctAnswer);
			
			finishedInputting = userYesOrNo("Have you finished? Enter y or any other key for no");	
		} while(!finishedInputting);
		
		int quizId = quizService.addQuiz(quiz);
		System.out.println();
		System.out.println("Quiz successfully added to quiz server");
		System.out.println("Quiz id is: " + quizId);
		mainMenu();	
	}
	
	
	//MENU METHODS START HERE
	
	/**
	 * Menu and prompt asking the user which quiz they would like to look at
	 *
	*/
	private void printQuizList() throws RemoteException{
	List<Quiz> quizList = quizService.getQuizzes();	
		printAllQuizzes(quizList);
		
		System.out.print("Which quiz would you like to look at: ");
		
		int option = getAndVerifyNumber(quizService.getQuizzesKeys());
		Quiz quizSelected = quizService.getQuiz(option);
		System.out.println();
		for(Question quizQuestion : quizSelected.getQuestions()){
			System.out.println("Question: " + quizQuestion.getQuestion());
			System.out.println();
			System.out.println(quizQuestion.getAnswers());	
			System.out.println("Correct answer is " + quizQuestion.getCorrectAnswer());	
			System.out.println();
			System.out.println();
		}
		mainMenu();
	}
	
	/**
	 * Main menu - the root menu asking user whether they want to create a quiz or view quizzes already on server
	 *
	*/
	private void mainMenu() throws RemoteException{
		System.out.println();
		System.out.println("***MENU***");
		System.out.println("1 - Create new quiz");
		System.out.println("2 - View list of quizzes on server");
		System.out.println("3 - Log off");
		System.out.println();
		System.out.print("Please enter your option: ");
		
		
		int option = getAndVerifyNumber(new Integer[]{1,2,3});
		
			switch(option){
				case 1:		createNewQuiz();
							break;
				case 2:		if(! quizService.getQuizzes().isEmpty()){
								printQuizList();
							} else {
								System.out.println();
								System.out.print("There are no quizzes currently on the server. Please choose another option: ");
								System.out.println();
								mainMenu();
							}
							break;
				case 3:		System.out.println("Goodbye");
							System.exit(0);
							break;
				default:	System.out.println("Invalid option");
							break;			
			}
	}
	
	/**
	 * Gets the quiz server the client is connected to
	 *
	 * @return the quiz server
	*/
	private QuizService getQuizService(){
		return quizService;
	} 
	
	/**
	 * Launch method launches main menu first to ensure menus are traversed in correct order
	 *
	 */
	private void launch() throws RemoteException{
		mainMenu();
	}
	
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException{
		
		try{
			new SetUpClientImpl().launch();
		} catch(IOException ex){
			System.out.println(ex);
		}
	}
}