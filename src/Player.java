
import java.util.List;

/**
 * A quiz player
 *
 * @author Julian Fenner
 */
public interface Player{
	/**
	 * Store a player's quiz attempt
	 *
	 * @param quizId the id of the quiz
	 * @param quizScore the score of the quiz attempt
	*/
	void addQuizAttempt(int quizId, int quizScore);
	
	/**
	 * Returns the player's id
	 * 
	 * @return the id of the player
	*/
	int getId();
	
	/**
	 * Returns the player's name
	 * 
	 * @return the name of the player
	*/
	String getName();
	
	/**
	 * Returns the list of quiz attempts of the player 
	 * 
	 * @return the list of quiz attempts, null if empty
	*/
	List<QuizAttempt> getQuizAttempts();
}