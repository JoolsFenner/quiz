
import java.rmi.RemoteException;

/**
 * A Client that connects to QuizServer and allows the player to set up a quiz
 *
*/
public interface SetUpClient{
	/**
	 * Creates a new quiz
	 *
	*/
	void createNewQuiz() throws RemoteException;	
}