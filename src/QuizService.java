

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * A class representing the quiz server.  Manages quiz and player data and client requests to update and access this
 *
 * @author Julian Fenner
 *
 */
public interface QuizService extends Remote {
	/**
	 * Add a new quiz to the server
	 *
	 * @param quiz the quiz to be added
	 * @return the ID for the quiz
	 * @throws NullPointerException if quiz is null
	*/
	int addQuiz(Quiz quiz) throws RemoteException;
	
	/**
	 * Returns all quizzes stored on the server
	 *
	 * @return a list of the quizzes 
	*/
	List<Quiz> getQuizzes() throws RemoteException;
	
	/**
	 * Returns the quiz with the requested ID or null if it there is none.
	 *
	 * @param quizId the ID for the quiz
	 * @return the quiz with the requested ID or null if it there is none.
	*/
	Quiz getQuiz(int quizId) throws RemoteException;
	
	/**
	 * Returns all the keys in the quizzes map or null if there are none
	 *
	 *@return array of all quiz keys or null if it there is none
	*/
	Integer[] getQuizzesKeys() throws RemoteException;
	
	/**
	 * Add a new player to the server
	 *
	 * @param name the name of the person to be added
	 * @return the ID for the person
	 * @throws IllegalArgumentException if name is empty 
	*/
	int createPlayer(String name) throws RemoteException;
	
	/**
	 * Returns all people stored on the server
	 *
	 * @return a list of the people 
	*/
	List<Player> getPlayers() throws RemoteException;
	
	/**
	 * Returns the player with the requested ID, or null if it there is none.
	 *
	 * @param playerId the ID for the quiz
	 * @return the player with the requested ID, or null if it there is none.
	*/
	Player getPlayer(int playerId) throws RemoteException;
	
	/**
	 * Adds a quiz score to a player's profile
	 *
	 *@param playerId the id of the player
	 *@param quizId the id of the quiz
	 *@param playerScore the quiz score obtained
	 *@throws IllegalArgumentException if playerId doesn't correspond to a player on the system
	 *@throws IllegalArgumentException if quizId doesn't correspond to a quiz on the system
	 *@throws IllegalArgumentException if score is less than zero of greater than maximum score of quiz
	*/
	void addPlayerScore(int playerId, int quizId, int playerScore) throws RemoteException;
		
	/**
	 * Returns all the keys in the player map or null if it there are none
	 *
	 *@return array of all player keys or null if it there are none
	*/
	Integer[] getPlayersKeys() throws RemoteException;
	
	/**
	 * Save all data to disk.
	 *
	 */
	void flush()throws RemoteException;
}