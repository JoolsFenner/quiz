

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;
import java.rmi.NotBoundException;
import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;	
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Implements the PlayerClient interface
 *
 * @author Julian Fenner
*/
public class PlayerClientImpl extends ClientTools implements PlayerClient{
	private Remote service;
	private QuizService quizService;
	private int playerLoggedOn;
		
	public PlayerClientImpl()throws NotBoundException, IOException{
		this.service = Naming.lookup("//127.0.0.1:1099/quiz");
		this.quizService = (QuizService) service;
	}
	
	@Override
	public void playQuiz(int quizId) throws RemoteException{
		Quiz q = quizService.getQuiz(quizId);
		
		List<Question> questions = q.getQuestions();
		int playerScore = 0;
		
		System.out.println();
		System.out.println("***" + q.getQuizName() +"***");	
		System.out.println();		
			
		for(Question next : questions){
			System.out.println(next.getQuestion());
			System.out.println();
			System.out.println(next.getAnswers());
			
			System.out.print("Input answer: ");
			
			int playerAnswer = getAndVerifyNumber(new Integer[]{1,2,3,4});
			
			System.out.println();
			if(next.getCorrectAnswer() == playerAnswer){
				System.out.println("YOU ARE CORRECT");
				playerScore++;
			} else{
				System.out.println("YOU ARE WRONG");
			}
						
			System.out.println("______________");	
			System.out.println();	
			System.out.println();	
		}
		System.out.println("You scored " + playerScore + " out of " + questions.size());
		
		quizService.addPlayerScore(playerLoggedOn, quizId, playerScore);
		playerOptionsMenu();
		
	}
	
	@Override
	public void showScores() throws RemoteException{
		List<QuizAttempt> playersAttempts = quizService.getPlayer(playerLoggedOn).getQuizAttempts();
		
		System.out.println();
		System.out.println("***" + quizService.getPlayer(playerLoggedOn).getName() + "'s quiz scores ***");
		System.out.println();
		
		for(QuizAttempt nextAttempt : playersAttempts){
			Quiz quiz = quizService.getQuiz(nextAttempt.getQuizId());

			System.out.println(quiz.getQuizName() + ": " +  nextAttempt.getQuizScore() + " out of " + quiz.getQuestions().size());
			System.out.println();			
		}
		playerOptionsMenu();
	}
	
	
	
	@Override
	public void createNewPlayer() throws RemoteException{
		System.out.println();
		System.out.println("***CREATE NEW PLAYER***");
		System.out.println();
		
		boolean confirmed = false;
		String newPlayerName;
		do{
			newPlayerName = getAndVerifyStringInput("Please enter your name: ");
			System.out.println();
			confirmed = userYesOrNo("You have entered your name as " + newPlayerName + ". Is this correct - y/n?: ");	
			
		}while (!confirmed);
			
		int playerId = quizService.createPlayer(newPlayerName);
		playerLoggedOn = playerId;
		System.out.println();
		System.out.println("Hi " + quizService.getPlayer(playerLoggedOn).getName() + "!");
		playerOptionsMenu();
	}
	
	
	//MENU METHODS START HERE
	
	/**
	 * Menu and prompt asking the player to choose a quiz to play
	 *
	*/
	private void chooseQuizMenu() throws RemoteException{
		List<Quiz> quizList = quizService.getQuizzes();	
		printAllQuizzes(quizList);
		
		System.out.print("Which quiz would you like to play: ");
		int quizId = getAndVerifyNumber(quizService.getQuizzesKeys());
		Quiz quizSelected = quizService.getQuiz(quizId);
		playQuiz(quizId);	
	}
	
	/**
	 * Menu and prompt asking the logged-on player what they want to do
	 *
	*/
	private void playerOptionsMenu()  throws RemoteException{
		System.out.println();
		System.out.println("***PLAYER OPTIONS MENU***");
		System.out.println();
		System.out.println("1 - Play Quiz");
		System.out.println("2 - See Scores");
		System.out.println("3 - Log off");
		System.out.println();
		System.out.print("Please enter your option: ");
		
		int option = getAndVerifyNumber(new Integer[]{1,2,3});
		
		switch(option){
			case 1:	if(! quizService.getQuizzes().isEmpty()){
							chooseQuizMenu();
						} else {
							System.out.println();
							System.out.print("There are no quizzes currently on the server. Please choose another option: ");
							System.out.println();
							playerOptionsMenu();
						}
						break;
			case 2:	if(quizService.getPlayer(playerLoggedOn).getQuizAttempts() !=null){
							showScores();
						} else{
							System.out.println();
							System.out.println("You haven't played any quizzes yet");
							playerOptionsMenu();
						}
						break;
			case 3:	System.exit(0);
			default:	System.out.println("Invalid option");
						break;			
		}
		
	}
	
	/**
	 * Menu showing list of player profiles saved on server and prompt asking player to select one.
	 *
	*/
	private void existingPlayerMenu() throws RemoteException{
		List<Player> playerList = quizService.getPlayers();
		
		System.out.println();
		System.out.println("*** CHOOSE PLAYER MENU***");
		System.out.println();
		
		for (Player nextplayer : playerList){
			System.out.println("Player: " + nextplayer.getId() + ": " + nextplayer.getName());
			System.out.println();
		}
		System.out.print("Which player are you?: ");
		
		int playerId = getAndVerifyNumber(quizService.getPlayersKeys());
		
		playerLoggedOn = playerId;
		System.out.println("Welcome back " +  quizService.getPlayer(playerLoggedOn).getName());
		playerOptionsMenu();	
	}
	
	/**
	 * Main menu - the root menu asking player if they are an existing or new player 
	 *
	*/
	private void mainMenu() throws RemoteException{
		System.out.println();
		System.out.println("***MAIN MENU - PLAYER CLIENT***");
		System.out.println();
		System.out.println("1 - Existing Player");
		System.out.println("2 - New Player");
		System.out.println("3 - Log off");
		System.out.println();
		System.out.print("Please enter your option: ");
		
		int option = getAndVerifyNumber(new Integer[]{1,2,3});
			
		switch(option){
			case 1:	if(! quizService.getPlayers().isEmpty()){
							existingPlayerMenu();
						} else{
							System.out.println();
							System.out.print("There are no saved players on the server. Please choose another option: ");
							System.out.println();
							mainMenu();
						}
						break;
			case 2:		createNewPlayer();
						break;
			case 3:		System.exit(0);
			default:	System.out.println("Invalid option");
						break;			
		}
		
	}
	
	/**
	 * Gets the quiz server the client is connected to
	 *
	 * @return the quiz server
	*/
	public QuizService getQuizService(){
		return quizService;
	}
	
	/**
	 * Launch method launches main menu first to ensure menus are traversed in correct order
	 *
	 */
	private void launch() throws RemoteException{
		mainMenu();
	}
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException{
		
		try{
			new PlayerClientImpl().launch();
		} catch(IOException ex){
			System.out.println(ex);
		}
	}
}