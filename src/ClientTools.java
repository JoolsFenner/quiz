

import java.util.List;

/**
 * An abstract class for use by all clients interfacing with quiz server. It contains methods 
 * to handle user input validation . It also contains a method to print all quizzes
 * 
 * @author Julian Fenner
*/
public abstract class ClientTools{
	/**
	 * Gets a yes or no response from the user. Returns a boolean value to indicate user response.
	 *
	 * @param question the question being asked
	 * @return true is user inputs 'y' and false if user inputs 'n'
	*/
	public boolean userYesOrNo(String question){
		System.out.print(question + ": ");
		
		while(true){
			String option = System.console().readLine().toLowerCase();
			
			switch(option){
				case "y":		return true;
								//break;
				case "n":		return false;
								//break;
				default:		System.out.print("Error input invalid. Try again: ");
								break;
			}
		}
	}
	
	
	/**
	 * Gets a non-empty string from the user
	 *
	 * @param question the question being asked
	 * @return non-empty string
	*/
	public String getAndVerifyStringInput(String question){	
		System.out.print(question + ": ");
		while(true){
			String option = System.console().readLine();
			if(option.length() > 0) return option;
			System.out.print("Error empty string. Try again: ");
		}
	}
	
	
/**
 * Ask a user for numerical input checking it against a list of valid inputs
 *
 * @param allValidInputs an array of valid inputs
 * @return the user's validated input
 * @throws NumberFormatException if input is not an integer
*/	
	public int getAndVerifyNumber(Integer[] allValidInputs){
		String userInput;
		boolean inputVerified = false;
		
		do{
			userInput = System.console().readLine();	
			
			try{			
				for (Integer validInput: allValidInputs){
					if(Integer.valueOf(userInput).equals(validInput)) inputVerified = true;
				}	 
			}catch (NumberFormatException e){
				System.out.println("Error, please enter an integer value");
			}
			
			if(!inputVerified) System.out.print("Error - Input invalid: ");
			
		} while(!inputVerified);
		
		return Integer.parseInt(userInput);
	}
	
/**
 * Prints a list of quizzes
 *
 * @param quizList a list of quizzes
*/	
	public void printAllQuizzes(List<Quiz> quizList){
	System.out.println();
		System.out.println("***VIEW ALL QUIZZES***");
		System.out.println();
		
		for (Quiz nextQuiz : quizList){
			System.out.println("Quiz: " + nextQuiz.getId() + ": " + nextQuiz.getQuizName());
			System.out.println();
		}
	}
}