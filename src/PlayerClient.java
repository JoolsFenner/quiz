
import java.rmi.RemoteException;

/**
* Client that connects to the QuizServer and allows the player to set up a profile and play quizzes
*
*/
public interface PlayerClient{
	
	/**
	 * Allows player to play quiz
	 *
	 * @param quizId the id of the quiz
	*/
	void playQuiz(int quizId) throws RemoteException;	
	
	/**
	 * Prints all the scores of the player's attempts so far
	 *
	*/
	void showScores() throws RemoteException;
	
	/**
	 * Creates a new player
	 *
	 */
	void createNewPlayer() throws RemoteException;
}