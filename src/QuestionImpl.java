

/**
 * Implements the Question interface
 *
 * @author Julian Fenner
*/

public class QuestionImpl implements Question, java.io.Serializable{
	private static final long serialVersionUID = 4L;
	private String question;
	private String[] answers;
	private int correctAnswer;
	
	public QuestionImpl(String question){
		this.question = question;
		this.answers = new String[4]; 
	}
	
	@Override
	public void setPotentialAnswer(int answerNumber, String answer){
		answerNumber -= 1; // -1 because answers are stored in array
		answers[answerNumber] = answer;
	}
	
	@Override
	public void setCorrectAnswer(int correctAnswer){
		this.correctAnswer = correctAnswer;
	}
	
	@Override
	public String getQuestion(){
		return question;
	}
	
	@Override
	public String getAnswers(){
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < answers.length; i++){
			builder.append("Answer " + (i+1) + ": " + answers[i] + System.getProperty("line.separator"));
		}
		return builder.toString();
	}
	
	@Override
	public int getCorrectAnswer(){
		return correctAnswer;
	}
	
}