

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.util.Set;

/**
 * Implements the QuizService interface
 *
 * @author Julian Fenner
*/
public class QuizServer extends UnicastRemoteObject implements QuizService, java.io.Serializable{
	
	private static final long serialVersionUID = 7L;
	
	private int quizId;
	private int playerId; 
	private Map<Integer, Quiz> quizzesMap;
	private Map<Integer, Player> playersMap;
	private final File SAVED_DATA;

	
	/**
	 * Constructor requires file name for data file.  This allows for test servers to be set up which won't conflict 
	 * with saved data of main server. If no argument given entered "ServerData.ser" assigned by default
	 *
	 *@param savedDataFileName is name of user's dataFile.  
	*/
	public QuizServer(String savedDataFileName) throws RemoteException {		
		this.quizId = 1;
		this.playerId = 1;	
		this.quizzesMap = new LinkedHashMap<Integer, Quiz>();
		this.playersMap = new LinkedHashMap<Integer, Player>();	
		this.SAVED_DATA = new File(savedDataFileName);
		
		if(fileExistsAndContainsData()) loadSavedData(); 
	}
	public QuizServer() throws RemoteException {
		this("ServerData.ser");
	}
	
	
	//Quiz Management Methods
	
	@Override
	public int addQuiz(Quiz quiz) throws RemoteException{
		int id = quiz.setId(quizId++);
		quizzesMap.put(id, quiz);
		return id;
	}
	
		
	@Override
	public List<Quiz> getQuizzes(){
		List<Quiz> quizList = new ArrayList<Quiz>();
		
		for(Quiz next : quizzesMap.values()){
			quizList.add(next);
		}
		return quizList;
	}
	
	
	@Override
	public Quiz getQuiz(int quizId){
		return quizzesMap.get(quizId);
	}
	
	
	@Override
	public Integer[] getQuizzesKeys(){
		Set<Integer> keys = quizzesMap.keySet();
		
		if(keys.isEmpty()) return null;
		
		Integer[] keyArray = (Integer[])(keys.toArray(new Integer[keys.size()]));
		return keyArray;
	}
	
	
	//Player management methods
	
	@Override
	public int createPlayer(String name){
		if(name.length()==0) throw new IllegalArgumentException("Error. No text in the String argument"); 
		Player player = new PlayerImpl(name, playerId++);
		int id = player.getId();
		playersMap.put(id, player);
		return id;
	}
	
	
	@Override
	public List<Player> getPlayers(){
		List<Player> playersList = new ArrayList<Player>();
		
		for(Player next : playersMap.values()){
			playersList.add(next);
		}
		return playersList;
	}
	

	@Override
	public Player getPlayer(int playerId){
		return playersMap.get(playerId);
	}
	
	
	@Override
	public void addPlayerScore(int playerId, int quizId, int playerScore){
		if(playersMap.get(playerId)==null) throw new IllegalArgumentException("Error. Player id doesn't correspond to player on the system"); 
		
		if(quizzesMap.get(quizId)==null) throw new IllegalArgumentException("Error. Quiz id doesn't correspond to quiz on the system"); 
		
		if (playerScore < 0 || playerScore > quizzesMap.get(quizId).getQuestions().size()) throw new IllegalArgumentException("Error. Score is not valid, outside of legal range"); 
	
		playersMap.get(playerId).addQuizAttempt(quizId, playerScore);
	}
	
	
	@Override
	public Integer[] getPlayersKeys(){
		Set<Integer> keys = playersMap.keySet();
		Integer[] keyArray = (Integer[])(keys.toArray(new Integer[keys.size()]));
		return keyArray;
	}
	
	
	//Persistence methods
	
	@Override
	public void flush() {

		ObjectOutputStream output = null;
		String fileName = SAVED_DATA.getName();
		try {
			output = new ObjectOutputStream(
						new BufferedOutputStream(
							new FileOutputStream(fileName)));
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		try {
			output.writeObject(this);
			System.out.println("Data saved successfully");
		} catch (IOException e) {
			System.out.println(e);
		}
		try {
			output.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	/**
	 * Reloads saved data from saved QuizServer file.  Does so loading file into a placeholder
	 * quizServer from which which the real quizServer's state is restored
	*/
	private void loadSavedData()	{
		ObjectInputStream inputStream = null;
		try{
			inputStream = new ObjectInputStream(
			new BufferedInputStream(
			new FileInputStream(SAVED_DATA)));
		} catch (FileNotFoundException e){
			System.out.println(e);
		} catch (EOFException e){
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}

		QuizServer placeHolderQuizServer = null;
		if(inputStream !=null){
		
			try{
				placeHolderQuizServer = (QuizServer)inputStream.readObject();
			} catch (IOException e) {
				System.out.println(e);
			} catch (ClassNotFoundException e){
				System.out.println(e);
			}
			try{
				inputStream.close();
			} catch (IOException e) {
				System.out.println(e);
			}
		
			try{
				this.quizId = placeHolderQuizServer.getQuizId();
				this.playerId = placeHolderQuizServer.getPlayerId();
				this.quizzesMap = placeHolderQuizServer.getQuizzesMap();
				this.playersMap = placeHolderQuizServer.getPlayersMap();
				System.out.println("Saved data loaded successfully");
			} catch (NullPointerException e){
			// Certain types of data corruption lead to null pointer exceptions being thrown here
				System.out.println("WARNING - Saved data not loaded, file may be corrupted");
			}
			
		} else{
			System.out.println("WARNING - Saved data not loaded, file may be corrupted");
		}
	}

	
	/**
	 * Tests whether data file exists and contains data
	 *
	 * @return true if file exists and contains data
	*/
	private boolean fileExistsAndContainsData(){
		if(SAVED_DATA.exists() && SAVED_DATA.length() > 0){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Returns the quiz ID - private method used for reloading data from file
	 *
	 * @return the quiz ID
	*/
	private int getQuizId(){
		return quizId;
	}
	
	
	/**
	 * Returns the player ID - private method used for reloading data from file
	 *
	 * @return the player ID
	*/
	private int getPlayerId(){
		return playerId;
	}
	
	/**
	 * Returns the quizzes map - private method used for reloading data from file
	 *
	 * @return the quizzes map
	*/
	private Map<Integer, Quiz> getQuizzesMap(){
		return quizzesMap;
	}
	
	/**
	 * Returns the players map - private method used for reloading data from file
	 *
	 * @return the players map
	*/
	private Map<Integer, Player> getPlayersMap(){
		return playersMap;
	}
}