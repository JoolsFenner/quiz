

import java.util.List;

/**
 * A class representing quizzes.  
 *
 * @author Julian Fenner
*/
public interface Quiz{
	
	/**
	 * Add a question to a quiz along with all potential answers and a number identifying the correct answer
	 *
	 *@param question the question
	 *@param answer1 possible answer 1
	 *@param answer2 possible answer 2
	 *@param answer3 possible answer 3
	 *@param answer4 possible answer 4
	 *@param correctAnswer the number of the correct answers corresponding to answer 1-4
	 *@throws IllegalArgumentException if correct answer is outside of the acceptable range
	*/
	void addQuestionAndAnswers(String question, String answer1, String answer2, String answer3, String answer4, int correctAnswer);
	
	/**
	 * Sets the quiz ID
	 *
	 * @param id the quiz id
	 * @return the quiz id
	*/
	int setId(int id);
	
	/**
	 * Gets the quiz ID
	 *
	 * @return the quiz id
	*/
	int getId();
	
	/**
	 * Gets a list of every question in the quiz
	 *
	 * @return the list of questions
	*/
	List<Question> getQuestions();
	
	/**
	 * Gets the name of the quiz
	 *
	 * @return the quiz name
	*/
	String getQuizName();
}