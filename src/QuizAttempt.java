

/**
 * A class representing quiz attempts storing the quiz attempted and the score obtained
 *
 * @author Julian Fenner
 */
public interface QuizAttempt{
	/**
	 * Gets the id the of the quiz attempt
	 *
	 * @return the quizId of the quiz attempt
	*/
	int getQuizId();
	
	/**
	 * Gets the score obtained in the quiz attempt
	 *
	 * @return the quiz attempt score
	*/
	int getQuizScore();	
}